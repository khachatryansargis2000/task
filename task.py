
"""
    Program  that converts numbers to Words
"""

from digits import digit1, digit2

__all__ = []


class NotSupportedNumber(Exception):
    def __init__(self, message="Number must be in range 0 to 999999999"):
        self.message = message
        super().__init__(self.message)


def __single(num: str) -> str:
    """
        Returns the single digit  number converted to word .
                Parameters:
                        num (str): string number
                Returns:
                        digit1[num] (str):  returns current single digit number in words
        """
    return digit1[num]


def __double(num: str) -> str:
    """
        Returns the two digit  number converted to word if it is possible  else calls single() function .
                Parameters:
                        num (str): string number
                Returns:
                        result (str):  returns current two digit number in words
                        single(num) (str): returns single(num) function result
        """
    if len(num) == 2:
        result = digit2[num[0]] + digit1[num[1]]
        return result
    return __single(num)


def __triple(num: str) -> str:
    """
        Returns the tree digit  number converted to word if it is possible  else calls double function .
                Parameters:
                        num (str): string number
                Returns:
                        result (str):  returns current tree digit number in words
                        double(num) (str): returns double(num) function result
        """
    if len(num) == 3:
        result = __single(num[0]) + ' հարյուր ' + __double(num[1:])
        return result
    return __double(num)


def __thousand(num: str, length: int) -> str:
    """
        Returns the tree digit  number converted to word if it is possible  else calls double function .
                Parameters:
                        num (str): string number
                        length (int): number digit cont
                Returns:
                       result:  returns current 4-6 digit number in words
        """
    result = __triple(num[:length - 3]) + ' հազար ' + __triple(num[length - 3:])
    return result


def main():
    try:
        number = input('Enter the number: ')
        number = int(number)  # check is number and deleting 0 from start
        if number < 0 or number > 10 ** 9 - 1:
            raise NotSupportedNumber
        number = str(number)
    except ValueError:
        print('Enter integer number ')
        main()
    else:
        num_length = len(number)
        if number == '0':
            print('Զրո')
        if num_length in range(1, 4):
            print(__triple(number))
        elif num_length in range(4, 7):
            print(__thousand(num=number, length=num_length))
        else:
            print(__triple(number[:num_length - 6]) + ' միլիոն', __thousand(num=number[num_length - 6:], length=6))


if __name__ == "__main__":
    main()
